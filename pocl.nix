{ pkgs ? import <nixpkgs> {} }:
with pkgs;
let
  llvmPackages = llvmPackages_11;
in
stdenv.mkDerivation rec {
  pname = "pocl";
  version = "1.6";
  src = fetchFromGitHub {
    owner = "pocl";
    repo = "pocl";
    rev = "v${version}";
    sha256 = "02w4xqhjsq2hb4n21sj7418h3l6h76wphrj32gvkj8l84yjn93wl";
  };

  cmakeFlags = [
    "-DENABLE_SPIR=OFF"
    # pocl's CMakeLists.txt expects xxxDIR variables to be relative
    # path, but nixpkgs set them to absolute path. Override it here.
    "-DCMAKE_INSTALL_BINDIR=bin"
    "-DCMAKE_INSTALL_INCLUDEDIR=include"
    "-DCMAKE_INSTALL_LIBDIR=lib"
  ];

  nativeBuildInputs = [
    cmake
    pkg-config
    llvmPackages.llvm
    llvmPackages.clang
    llvmPackages.clang-unwrapped # for libclang headers
    llvmPackages.libclang
    opencl-headers
  ];
  buildInputs = [
    ocl-icd
    hwloc
  ];
  propagatedBuildInputs = [
    libgcc
  ];
}
