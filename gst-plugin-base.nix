{ pkgs ? import <nixpkgs> {} }:
with pkgs;
stdenv.mkDerivation rec {
  pname = "gst-plugins-base";
  version = "1.8.1";

  src = fetchurl {
    url = "http://gstreamer.freedesktop.org/src/${pname}/${pname}-${version}.tar.xz";
    sha256 = "0vxd5w7r1jqp37cw5lhyc6vj2h6z8y9v3xarwd2c6rfjbjcdxa8m";
  };

  enableParallelBuilding = true;

  nativeBuildInputs = [
    pkgconfig
    python
    gnumake42
  ];

  buildInputs = [
    gstreamer
  ];

}
