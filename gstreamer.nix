{ pkgs ? import <nixpkgs> {} }:
with pkgs;
stdenv.mkDerivation rec {
  pname = "gstreamer";
  version = "1.8.2";

  src = fetchurl {
    url = "http://gstreamer.freedesktop.org/src/gstreamer/${pname}-${version}.tar.xz";
    sha256 = "9dbebe079c2ab2004ef7f2649fa317cabea1feb4fb5605c24d40744b90918341";
  };

  configureFlags = [
    "--with-bash-completion-dir=no" # Adds gcc to closure size
  ];

  enableParallelBuilding = true;

  nativeBuildInputs = [
    pkgconfig perl bison flex python # gobject-introspection
  ];

  propagatedBuildInputs = [ glib ];

}
