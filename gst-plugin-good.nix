{ pkgs ? import ./nixpkgs {} }:
with pkgs;
stdenv.mkDerivation rec {
  pname = "gst-plugins-good";
  version = "1.8.1";

  src = fetchurl {
    url = "http://gstreamer.freedesktop.org/src/${pname}/${pname}-${version}.tar.xz";
    sha256 = "2103e17921d67894e82eafdd64fb9b06518599952fd93e625bfbc83ffead0972";
  };

  enableParallelBuilding = true;

  nativeBuildInputs = [
    pkgconfig
    python
    gnumake42
  ];

  buildInputs = [
    gstreamer
    gst-plugins-base
  ];
}
