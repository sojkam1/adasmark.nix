{ pkgs ? import <nixpkgs> {} }:
with pkgs;
stdenv.mkDerivation rec {
  pname = "imx-gpu-viv";
  version = "6.4.3.p1.0";
  src = fetchurl {
    url = "https://www.nxp.com/lgfiles/NMG/MAD/YOCTO/${pname}-${version}-aarch64.bin"; # ;fsl-eula=true
    sha256 = "0505620c3851a980d2e08fee4b4fcd06eff92efac02d1646924bea86e4384ad4";
  };

  nativeBuildInputs = [ autoPatchelfHook ];
  buildInputs = [
    libdrm
    stdenv.cc.cc.lib # libstdc++.so.6
    wayland # libwayland-server.so.0
  ] ++ (with xlibs; [
    libXdamage
    libX11
    libXext
    libXfixes
  ]);

  unpackCmd = "sh $curSrc --auto-accept --force";

  # Variables for install script copied from Yocto
  S = ".";
  D = "${placeholder "out"}/";
  bindir = "bin";
  includedir = "include";
  libdir = "lib";
  sysconfdir = "etc";
  HAS_GBM = "false";
  IS_MX8 = 1;
  USE_WL = "yes";
  USE_X11 = "no";

  installPhase = ''
    sh -x -e ${./install.sh}
  '';

  meta = {
    description = "GPU driver and apps for i.MX";
    #license = licenses.unfree;
  };
}
