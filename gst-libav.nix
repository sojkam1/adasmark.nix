# { pkgs ? import ./nixpkgs {}


{
  sources ? import ./nix/sources.nix,
  pkgs ? import sources.nixpkgs {
    overlays = [
      (self: super: {
        gstreamer = (import ./gstreamer.nix { pkgs = self; });
        gst-plugins-base = (import ./gst-plugin-base.nix { pkgs = self; });
        gst-plugins-good = (import ./gst-plugin-good.nix { pkgs = self; });
      })
    ];
  }
}:

with pkgs;
stdenv.mkDerivation rec {
  pname = "gst-libav";
  version = "1.8.1";

  src = fetchurl {
    url = "http://gstreamer.freedesktop.org/src/${pname}/${pname}-${version}.tar.xz";
    sha256 = "44a49108c3531b5ac4f346a2247cd7fbafb0e8ab394394cb6d75a70300b38933";
  };

  enableParallelBuilding = true;

  configureFlagsArray = [
    "--disable-yasm"
  ];

  nativeBuildInputs = [
    pkgconfig
    python
    gnumake42
  ];

  buildInputs = [
    gstreamer
    gst-plugins-base
    gst-plugins-good
  ];
}
