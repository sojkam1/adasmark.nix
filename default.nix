#
{
  sources ? import ./nix/sources.nix,
  pkgs ? import sources.nixpkgs {
    overlays = [
      (self: super: {
        gstreamer = (import ./gstreamer.nix { pkgs = self; });
        gst-plugins-base = (import ./gst-plugin-base.nix { pkgs = self; });
        gst-plugins-good = (import ./gst-plugin-good.nix { pkgs = self; });
 #       gst-libav = (import ./gst-libav.nix { pkgs = self; });
      })
    ];
    # crossSystem = { config = "aarch64-unknown-linux-gnu"; };
  },
  shell ? false
}:
with pkgs;
let
  inherit (lib.strings)
    concatStringsSep
    ;
  # Unpacking the adasmark tarball takes long time. To make
  # experimenting with adasmark faster, store unpacked copy in the nix
  # store.
  src = stdenv.mkDerivation {
    name = "adasmark-unpacked";
    src = requireFile {
      name = "adasmark-1.0.1.tar.gz";
      sha256 = "3a93d56327051b794332f095e9a863d8ccf9eced7140aa9800c3976176f319bf";
      url = "ssh://git@rtime.felk.cvut.cz/benchmarks";
    };
    buildCommand = "tar xf $src && mv adasmark-1.0.1 $out";
  };
  hostPlatformName = if hostPlatform.isx86_64 then "x86_64"
                     else if hostPlatform.isAarch64 then "aarch64"
                     else if hostPlatform.isAarch32 then "aarch32"
                     else "unsupported-platform";
  # Install auzone to a separate derivation in order to properly find
  # Nix-provided libOpenCL.so. This is achieved by autoPatchelfHook,
  # which modifies the supplied binary in the course of derivation
  # build.
  auzone = stdenv.mkDerivation {
    name = "auzone";
    inherit src;
    dontConfigure = true;
    dontBuild = true;
    installPhase = "cp -v -a $src/auzone_tsr/dist/${hostPlatformName} $out";
    nativeBuildInputs = [ autoPatchelfHook ];
    buildInputs = [ ocl-icd ];
  };
  adasmark = stdenv.mkDerivation rec {
    name = "adasmark-1.0.1";
    pname = "adasmark";
    version = "1.0.1";

    inherit src;

    enableParallelBuilding = true;

    configureFlagsArray = [
      "--with-auzone-prefix=${auzone}"
    ];

    nativeBuildInputs = [
      pkgconfig
    ];

    buildInputs = [
      opencl-headers
      ocl-icd
      gstreamer
      gst-plugins-base
      gst-plugins-good
      #gst-libav
      libxml2
      auzone
      libpng
    ];

    # Make it the output suitable for thermobench --column
    postPatch = ''
      substituteInPlace src/gstcnnterminator.c --replace "Processed frame: " "Processed frame="
    '';

    postInstall = ''
      cp $src/launcher/run_pipe.sh $out/bin
      substituteInPlace $out/bin/run_pipe.sh \
        --replace ./pbuilder $out/bin/pbuilder \
        --replace .libs/launcher $out/bin/launcher
    '';
  };
  gst-plugins = [
    "${gstreamer}/lib"
    "${gst-plugins-base}/lib"
    "${gst-plugins-good}/lib"
    "${adasmark}/lib/adasmark"
  ];
  adasmark-wrapped = stdenv.mkDerivation {
    name = "adasmark-wrapped";
    nativeBuildInputs = [ makeWrapper ];
    buildCommand = ''
      mkdir -p $out/bin
      for i in ${adasmark}/bin/{pbuilder,launcher,run_pipe.sh}; do
        makeWrapper $i $out/bin/''${i##*/} \
                    --set ADASMARK_KERNELS_LOCATION ${src}/kernels \
                    --set ADASMARK_MEDIA_LOCATION ${src}/media \
                    --set GST_PLUGIN_PATH ${concatStringsSep ":" gst-plugins}
      done
    '';
  };
  pocl = (import ./pocl.nix { inherit pkgs; });
  adasmark-shell = mkShell {
    ADASMARK_KERNELS_LOCATION = "${src}/kernels";
    ADASMARK_MEDIA_LOCATION = "${src}/media";
    GST_PLUGIN_PATH = "${concatStringsSep ":" gst-plugins}";
    shellHook = ''
      export ADASMARK_ROOT="$PWD"
      mkdir -p "$ADASMARK_ROOT/launcher/cfg/"
      cp --verbose --no-clobber --no-preserve=mode,ownership ${src}/launcher/cfg/* "$ADASMARK_ROOT/launcher/cfg/"
    '';

    buildInputs = [
      adasmark
      gstreamer
      bashInteractive

      # This makes libgcc available in the shell; it is needed for
      # running programs with PoCL. More Nixy way would be to force
      # pocl linker to use absolute path to libgcc rather than
      # `-lgcc`, but this would require digging deeply into how clang
      # works, so we leave it for later :-)
      pocl
    ];
    OCL_ICD_VENDORS="${pocl}/etc/OpenCL/vendors/";
  };
in
if !shell then adasmark-wrapped else adasmark-shell
