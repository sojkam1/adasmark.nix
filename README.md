# Usage

1. [Install Nix](https://nixos.org/download.html#download-nix).

   Note: To install Nix on i.MX8 running Yocto, extra steps are needed
   as Nix officially does not support installation by the root user.
   The following sequence of commands installed Nix on Yocto for me:

        curl -o install-nix https://releases.nixos.org/nix/nix-2.13.2/install
		groupadd --system nixbld
		useradd --system nixbld1 -G nixbld
		sh ./install-nix

2. Get ADASMark sources:

        git clone git@rtime.felk.cvut.cz:benchmarks
        cd benchmarks/EEMBC/ADASMark
		git annex get adasmark-1.0.1.tar.gz

3. Add ADASMark sources to Nix store:

        nix-prefetch-url --type sha256 file://$PWD/adasmark-1.0.1.tar.gz

4. Clone this repo:

        git clone .../adasmark.nix
		cd adasmark.nix

5. Build everything:

        nix-build

6. You can use the binaries that appear in `./result/bin` but it is
   preferable to use `nix-shell`, which sets all needed environment
   variables and copies configuration to the current directory for
   their easy modification:

        nix-shell
		run_pipe.sh

   The above command will use PoCL OpenCL platform since `nix-shell`
   sets the environment variable `OCL_ICD_VENDORS`. If you want to use
   system OpenCL platform (which would work only on NixOS), unset the
   environment variable before running adasmark:

        unset OCL_ICD_VENDORS

   To run Nix-compiled ADASMark on GPU on Yocto-based i.MX8, it is necessary
   to manually preload OpenCL libraries that are not in the Nix store.
   Thus, the benchmark can be run from `nix-shell` by running:

   ```sh
   LD_PRELOAD="/usr/lib/libOpenCL.so.1 /usr/lib/libEGL.so.1 /usr/lib/libGAL.so /usr/lib/libGLESv2.so.2 /usr/lib/libVSC.so /usr/lib/libdrm.so.2 /usr/lib/libffi.so.7 /usr/lib/libgbm.so.1 /usr/lib/libgbm_viv.so /usr/lib/libwayland-client.so.0 /usr/lib/libwayland-server.so.0 /usr/lib/libCLC.so" \
     launcher -i launcher/cfg/base-single
   ```

   If you want run it on CPU, from `nix-shell` do
   ```sh
   launcher -i launcher/cfg/base-single
   ```

   Of course, the configuration (`-i` argument) can be changed. Also can be added argument `-l` which run source video in an infinite loop. And you can limit number of used frames from input video with argument `-f number`. This will use first `number` of frames from input. This can be combined with looping.
